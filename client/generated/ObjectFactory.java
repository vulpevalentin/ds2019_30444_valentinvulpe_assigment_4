
package com.client.generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.client.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPatientActivityResponse_QNAME = new QName("http://service.main/", "getPatientActivityResponse");
    private final static QName _GetPatientActivity_QNAME = new QName("http://service.main/", "getPatientActivity");
    private final static QName _GetAllResponse_QNAME = new QName("http://service.main/", "getAllResponse");
    private final static QName _GetActivity_QNAME = new QName("http://service.main/", "getActivity");
    private final static QName _GetActivityResponse_QNAME = new QName("http://service.main/", "getActivityResponse");
    private final static QName _GetAll_QNAME = new QName("http://service.main/", "getAll");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.client.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetActivity }
     * 
     */
    public GetActivity createGetActivity() {
        return new GetActivity();
    }

    /**
     * Create an instance of {@link GetActivityResponse }
     * 
     */
    public GetActivityResponse createGetActivityResponse() {
        return new GetActivityResponse();
    }

    /**
     * Create an instance of {@link GetAll }
     * 
     */
    public GetAll createGetAll() {
        return new GetAll();
    }

    /**
     * Create an instance of {@link GetAllResponse }
     * 
     */
    public GetAllResponse createGetAllResponse() {
        return new GetAllResponse();
    }

    /**
     * Create an instance of {@link GetPatientActivity }
     * 
     */
    public GetPatientActivity createGetPatientActivity() {
        return new GetPatientActivity();
    }

    /**
     * Create an instance of {@link GetPatientActivityResponse }
     * 
     */
    public GetPatientActivityResponse createGetPatientActivityResponse() {
        return new GetPatientActivityResponse();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientActivityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getPatientActivityResponse")
    public JAXBElement<GetPatientActivityResponse> createGetPatientActivityResponse(GetPatientActivityResponse value) {
        return new JAXBElement<GetPatientActivityResponse>(_GetPatientActivityResponse_QNAME, GetPatientActivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientActivity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getPatientActivity")
    public JAXBElement<GetPatientActivity> createGetPatientActivity(GetPatientActivity value) {
        return new JAXBElement<GetPatientActivity>(_GetPatientActivity_QNAME, GetPatientActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getAllResponse")
    public JAXBElement<GetAllResponse> createGetAllResponse(GetAllResponse value) {
        return new JAXBElement<GetAllResponse>(_GetAllResponse_QNAME, GetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getActivity")
    public JAXBElement<GetActivity> createGetActivity(GetActivity value) {
        return new JAXBElement<GetActivity>(_GetActivity_QNAME, GetActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getActivityResponse")
    public JAXBElement<GetActivityResponse> createGetActivityResponse(GetActivityResponse value) {
        return new JAXBElement<GetActivityResponse>(_GetActivityResponse_QNAME, GetActivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.main/", name = "getAll")
    public JAXBElement<GetAll> createGetAll(GetAll value) {
        return new JAXBElement<GetAll>(_GetAll_QNAME, GetAll.class, null, value);
    }

}
