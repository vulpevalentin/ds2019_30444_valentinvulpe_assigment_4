package main.service;

import com.client.generated.Activity;
import com.client.generated.ActivityServiceImplService;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityService {

    public List<Activity> getAll(){
        return getServiceProxy().getAll();
    }

    public List<Activity> getPatientActivity(Integer id){
        return getServiceProxy().getPatientActivity(id);
    }

    public Activity getActivity(Integer id){
        return getServiceProxy().getActivity(id);
    }

    private com.client.generated.ActivityService getServiceProxy(){
        com.client.generated.ActivityService activityServiceProxy = null;
        try {
            URL url = new URL("http://localhost:9901/activity?wsdl");
            ActivityServiceImplService activityServiceImplService
                    = new ActivityServiceImplService(url);
            activityServiceProxy = activityServiceImplService.getActivityServiceImplPort();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return activityServiceProxy;
    }
}
