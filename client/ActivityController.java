package main.controller;

import com.client.generated.Activity;
import main.dto.View.UserViewDTO;
import main.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Controller
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/activity")
public class ActivityController {

    private ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService){

        this.activityService = activityService;
        //test();
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    List<Activity> getAll(){
//        System.out.println("/activity/all");
        return activityService.getAll();
    }


    @GetMapping("/patient")
    public @ResponseBody List<Activity> findByUserId(@RequestParam(name="id") String id){
//        System.out.println("/activity/patient + "+id);
        List<Activity> list = new ArrayList<>();

        if(!id.equals("null")){
            list = activityService.getPatientActivity(Integer.parseInt(id));
        }
        return list;
    }

    private void test(){
        List<Activity> activityList = getAll();
        for(Activity activity:activityList){
            System.out.println(activity.getActivityName());
        }

    }


}
