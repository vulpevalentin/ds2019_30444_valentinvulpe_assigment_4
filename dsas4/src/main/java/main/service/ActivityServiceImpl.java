package main.service;

import main.model.Activity;
import main.repository.ActivityRepository;
import main.repository.ActivityRepositoryImpl;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService( endpointInterface = "main.service.ActivityService")
public class ActivityServiceImpl implements AbnormalActivityService{

    @Inject
    private ActivityRepository activityRepositoryImpl;

    public ActivityServiceImpl (){
        activityRepositoryImpl = new ActivityRepositoryImpl();
    }


    @WebMethod
    public Activity getActivity(int id){
        Activity activity = activityRepositoryImpl.getActivity(id);
        return activity;
    }

    @WebMethod
    public List<Activity> getPatientActivity(int patientId){
        return activityRepositoryImpl.getPatientActivity(patientId);
    }

    @WebMethod
    public List<Activity> getAll(){
        return activityRepositoryImpl.getAll();
    }

//    @WebMethod
//    List<Activity> getAll(){
//        return activityRepositoryImpl.getAll();
//    }
}
