package main.service;

import main.model.Activity;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ActivityService {
    @WebMethod
    Activity getActivity(int id);

//    @WebMethod
//    Activity addActivity(int id, String name);

    @WebMethod
    List<Activity> getPatientActivity(int patientId);

//    @WebMethod
//    List<Activity> getAll();
    @WebMethod
    List<Activity> getAll();



}
