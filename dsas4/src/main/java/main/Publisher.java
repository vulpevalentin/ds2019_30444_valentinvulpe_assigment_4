package main;

import main.model.Activity;
import main.repository.ActivityRepositoryImpl;
import main.service.ActivityServiceImpl;

import javax.xml.ws.Endpoint;

public class Publisher {
    public static void main(String[] args) {
        System.out.println("Server Started!");
        System.out.println("http://localhost:9901/activity");
        Endpoint.publish("http://localhost:9901/activity", new ActivityServiceImpl());
    }
}
