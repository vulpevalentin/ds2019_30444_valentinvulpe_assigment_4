package main.repository;

import main.model.Activity;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ActivityRepositoryImpl implements ActivityRepository {
    private Connection connection;


    public ActivityRepositoryImpl(){
        String url = "jdbc:mysql://localhost:3306/ds_assig1";
        String user = "root";
        String pass = "root";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url,user,pass);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Activity getActivity(int id){

        java.text.SimpleDateFormat format =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from activity where " +
                            "id = " + id
            );

            resultSet.next();
            Activity activity = new Activity(
                    resultSet.getInt("patient_id"),
                    format.parse(resultSet.getString("timeStart")),
                    format.parse(resultSet.getString("timeEnd")),
                    resultSet.getString("name")
            );
            return activity;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Activity> getPatientActivity(int patientId){
        List<Activity> list = new ArrayList<Activity>();
        try {

            java.text.SimpleDateFormat format =
                    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from activity where " +
                            "patient_id = " + patientId
            );
            while (resultSet.next()){
                Activity activity = new Activity(
                        resultSet.getInt("patient_id"),
                        format.parse(resultSet.getString("timeStart")),
                        format.parse(resultSet.getString("timeEnd")),
                        resultSet.getString("name")
                );
                list.add(activity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Activity> getAll(){
        List<Activity> list = new ArrayList<Activity>();
        try {

            java.text.SimpleDateFormat format =
                    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from activity"
            );
            while (resultSet.next()){
                Activity activity = new Activity(
                        resultSet.getInt("patient_id"),
                        format.parse(resultSet.getString("timeStart")),
                        format.parse(resultSet.getString("timeEnd")),
                        resultSet.getString("name")
                );
                list.add(activity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }




}
