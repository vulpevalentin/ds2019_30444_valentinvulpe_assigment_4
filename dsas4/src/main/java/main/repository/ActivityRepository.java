package main.repository;

import main.model.Activity;

import java.util.List;

public interface ActivityRepository {
    List<Activity> getPatientActivity(int patientId);

    Activity getActivity(int id);

    List<Activity> getAll();
}
