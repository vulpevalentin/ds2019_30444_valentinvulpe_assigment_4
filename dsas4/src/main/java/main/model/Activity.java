package main.model;



import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Activity implements Serializable {
    private Integer patientID;
    private Date timeStart;
    private Date timeEnd;
    private String activityName;

    public Activity(Integer patientID, Date timeStart, Date timeEnd, String activityName) {
        this.patientID = patientID;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.activityName = activityName;
    }

    public Activity(){}



    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return "patientID: " + patientID+
                " ,timeStart: " + dateFormat.format(timeStart) +
                " ,timeEnd: " + dateFormat.format(timeEnd) +
                " ,activityName: " + activityName ;
    }
}