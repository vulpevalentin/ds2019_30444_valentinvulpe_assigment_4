package main.model;

import java.io.Serializable;
import java.util.Date;

public class AbnormalActivity implements Serializable {


    private Integer patientID;
    private Date timeStart;
    private Date timeEnd;
    private String activityName;
    private Boolean abnormal;
    private String recommendation;

    public AbnormalActivity(){}

    public AbnormalActivity(Integer patientID, Date timeStart, Date timeEnd, String activityName, Boolean abnormal, String recommendation) {
        this.patientID = patientID;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.activityName = activityName;
        this.abnormal = abnormal;
        this.recommendation = recommendation;
    }

    public AbnormalActivity(Integer patientID, Date timeStart, Date timeEnd, String activityName, Boolean abnormal) {
        this.patientID = patientID;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.activityName = activityName;
        this.abnormal = abnormal;
    }

    public AbnormalActivity(Integer patientID, Date timeStart, Date timeEnd, String activityName) {
        this.patientID = patientID;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.activityName = activityName;
    }

    public AbnormalActivity(Activity activity,  Boolean abnormal, String recommendation){
        this.patientID = activity.getPatientID();
        this.timeStart = activity.getTimeStart();
        this.timeEnd = activity.getTimeEnd();
        this.activityName = activity.getActivityName();
        this.abnormal = abnormal;
        this.recommendation = recommendation;
    }

    public AbnormalActivity(Activity activity,  Boolean abnormal){
        this.patientID = activity.getPatientID();
        this.timeStart = activity.getTimeStart();
        this.timeEnd = activity.getTimeEnd();
        this.activityName = activity.getActivityName();
        this.abnormal = abnormal;
    }

    public AbnormalActivity(Activity activity){
        this.patientID = activity.getPatientID();
        this.timeStart = activity.getTimeStart();
        this.timeEnd = activity.getTimeEnd();
        this.activityName = activity.getActivityName();
    }



    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Boolean getAbnormal() {
        return abnormal;
    }

    public void setAbnormal(Boolean abnormal) {
        this.abnormal = abnormal;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}
